﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	
	public GameObject wave;
	public float interval = 1.0f;
	public float rotx;
	public float roty;
	public float rotz;
	private GameObject obj;

	// Use this for initialization
	void Start () {
		Spawn();
	}
	
	void Spawn()
	{
		Vector3 pos = new Vector3(-50.0F, Random.Range(-10.0F, 20.0F), Random.Range(-15.0F, 15.0F));
		Vector3 rot = new Vector3(rotx, roty, rotz);
		obj = (GameObject)Instantiate(wave, pos, this.transform.rotation);
		obj.transform.Rotate (rot);
		Invoke("Spawn", interval);
	}
}