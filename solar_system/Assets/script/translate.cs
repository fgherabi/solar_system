﻿using UnityEngine;
using System.Collections;


public class translate : MonoBehaviour {

	public float speed;
	public float x;
	public float y;
	public float z;

	// Update is called once per frame
	void FixedUpdate () {
		transform.Translate(new Vector3(x, y, z) * speed, Space.World);
	}
}
