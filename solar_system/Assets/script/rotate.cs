﻿using UnityEngine;
using System.Collections;

public class rotate : MonoBehaviour {

	public float rotationSpeed;
	public float x;
	public float y;
	public float z;

	void FixedUpdate ()
	{
		transform.Rotate(new Vector3(x, y, z) * rotationSpeed);
	}
}
