
enum CONDITION
{
	OUT_LEFT,
	OUT_RIGHT,
	NO_CHILD,
	OUT_OF_SCREEN
}

var condition:CONDITION;

function Update () {
	var cam = Camera.mainCamera;
	var posInScreen = cam.WorldToViewportPoint(this.transform.position);

	switch (condition) {
		case CONDITION.OUT_LEFT :
			if (posInScreen.x < -0.5)
				Destroy(this.gameObject);
		break;
		case CONDITION.OUT_RIGHT :
			if (posInScreen.x > 1.5)
				Destroy(this.gameObject);
		break;
		case CONDITION.NO_CHILD :
			if (this.transform.childCount == 0)
				Destroy(this.gameObject);
		break;
		case CONDITION.OUT_OF_SCREEN :

		break;
	}
}